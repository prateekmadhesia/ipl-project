// matches per year
fetch("output/matchesPerYear.json")
  .then((response) => response.json())
  .then((data) => {
    let key = Object.keys(data);
    let values = Object.values(data);

    Highcharts.chart("matchesPerYear", {
      chart: {
        type: "column",
      },
      title: {
        text: "IPL Matches Per Year",
      },
      xAxis: {
        categories: key, //data transfer as an array
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "No. Of Matches",
        },
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:13px">In {point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">Total Matches: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: "Year",
          data: values, //data transfer as an array
        },
      ],
    });
  });

// Match won per team per year
fetch("output/matchesWonPerYear.json")
  .then((response) => response.json())
  .then((data) => {
    let newData = matchWonHelper(data);
    return newData;
  })
  .then((newData) => {
    Highcharts.chart("matchesWonPerYear", {
      chart: {
        type: "bar",
      },
      title: {
        text: "Matches Won Per Year",
      },
      xAxis: {
        categories: newData.key,
        title: {
          text: "Year",
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "Number of matches",
          align: "high",
        },
        labels: {
          overflow: "justify",
        },
      },
      tooltip: {
        valueSuffix: " millions",
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true,
          },
        },
      },
      legend: {
        layout: "vertical",
        align: "right",
        verticalAlign: "top",
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || "#FFFFFF",
        shadow: true,
      },
      credits: {
        enabled: false,
      },
      series: newData.values,
    });
  });

// Extra runs conceded per team
fetch("output/extraRunsIn2016.json")
  .then((response) => response.json())
  .then((data) => {
    let elements = Object.entries(data);
    Highcharts.chart("extraRunsIn2016", {
      chart: {
        type: "column",
      },
      title: {
        text: "Extra Runs In 2016",
      },
      xAxis: {
        type: "category",
        labels: {
          rotation: -55,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif",
          },
        },
      },
      yAxis: {
        min: 0,
        title: {
          text: "Runs",
        },
      },
      legend: {
        enabled: false,
      },
      tooltip: {
        pointFormat: "Extra runs: <b>{point.y}</b>",
      },
      series: [
        {
          name: "Population",
          data: elements,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: "Black",
            align: "right",
            format: "{point.y}", // one decimal
            y: 10, // 10 pixels down from the top
            style: {
              fontSize: "13px",
              fontFamily: "Verdana, sans-serif",
            },
          },
        },
      ],
    });
  });

// Top 10 economical bowlers in the year 2015
fetch("output/top10EconomicalBowlers.json")
  .then((response) => response.json())
  .then((data) => {
    let key = Object.keys(data);
    let values = Object.values(data);

    Highcharts.chart("top10EconomicBowler", {
      chart: {
        type: "column",
      },
      title: {
        text: "Top 10 Economic Bowler",
      },
      xAxis: {
        categories: key, //data transfer as an array
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "Economic",
        },
      },
      tooltip: {
        headerFormat:
          '<span style="font-size:13px">In {point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">Economic: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: "Players",
          data: values, //data transfer as an array
        },
      ],
    });
  });

// Toss And Match Won
fetch("output/tossAndMatchWon.json")
  .then((response) => response.json())
  .then((data) => {
    let newData = Object.entries(data);
    Highcharts.chart("tossAndMatchWon", {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: "pie",
      },
      title: {
        text: "Toss And Match Won",
      },
      tooltip: {
        pointFormat:
          "{series.name}: <b>{point.y} Tosses as well as Matches</b>",
      },
      accessibility: {
        point: {
          valueSuffix: "%",
        },
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: "pointer",
          dataLabels: {
            enabled: true,
            format: "<b>{point.name}</b>: {point.y}",
          },
        },
      },
      series: [
        {
          name: "Won",
          colorByPoint: true,
          data: newData,
        },
      ],
    });
  });

//player who has won the highest number of Player of the Match awards for each season
fetch("output/playerOfTheMatch.json")
  .then((response) => response.json())
  .then((data) => {
    let newData = playerOfTheMatchHelper(data);
    return newData;
  })
  .then((data) => {
    Highcharts.chart("playerOfTheMatch", {
      chart: {
        type: "column",
      },
      title: {
        text: "IPL Matches Per Year",
      },
      xAxis: {
        categories: data.keys, //data transfer as an array
        crosshair: true,
      },
      yAxis: {
        min: 0,
        title: {
          text: "No. Of Matches",
        },
      },
      tooltip: {
        headerFormat: '<span style="font-size:13px">{point.key}</span><table>',
        pointFormat:
          '<tr><td style="color:{series.color};padding:0">Total Matches: </td>' +
          '<td style="padding:0"><b>{point.y}</b></td></tr>',
        footerFormat: "</table>",
        shared: true,
        useHTML: true,
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0,
        },
      },
      series: [
        {
          name: data.names,
          colorByPoint: true,
          data: data.values, //data transfer as an array
        },
      ],
    });
  });

//highest number of times one player has been dismissed by another player
fetch("output/playerDismiss.json")
  .then((response) => response.json())
  .then((data) => {
    let newData = Object.entries(data);
    Highcharts.chart("playerDismiss", {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
      },
      title: {
        text: "Player<br>Dismiss",
        align: "center",
        verticalAlign: "middle",
        y: 60,
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y}</b>",
      },
      accessibility: {
        point: {
          valueSuffix: "%",
        },
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: "bold",
              color: "white",
            },
          },
          startAngle: -90,
          endAngle: 90,
          center: ["50%", "75%"],
          size: "110%",
        },
      },
      series: [
        {
          type: "pie",
          name: "Player Dismiss",
          innerSize: "50%",
          data: newData,
        },
      ],
    });
  });

//the best economy in super overs
fetch("output/economySuperOver.json")
  .then((response) => response.json())
  .then((data) => {
    let newData = Object.entries(data);
    newData[0][1] = newData[0][1].average;
    Highcharts.chart("economySuperOver", {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
      },
      title: {
        text: "Player<br>Dismiss",
        align: "center",
        verticalAlign: "middle",
        y: 60,
      },
      tooltip: {
        pointFormat: "{series.name}: <b>{point.y}</b> ",
      },
      accessibility: {
        point: {
          valueSuffix: "%",
        },
      },
      plotOptions: {
        pie: {
          dataLabels: {
            enabled: true,
            distance: -50,
            style: {
              fontWeight: "bold",
              color: "white",
            },
          },
          startAngle: -90,
          endAngle: 90,
          center: ["50%", "75%"],
          size: "110%",
        },
      },
      series: [
        {
          type: "pie",
          name: "SuperOver Economy",
          innerSize: "50%",
          data: newData,
        },
      ],
    });
  });

function matchWonHelper(data) {
  let key = Object.keys(data);
  let values = [];
  let allTeams = [];

  // Get all team name
  for (let year in data) {
    for (let team in data[year]) {
      let find = allTeams.find((value) => {
        return value == team;
      });
      if (find == undefined) {
        allTeams.push(team);
      }
    }
  }
  allTeams.sort();

  //created a blueprint for values
  for (let team of allTeams) {
    let newObject = {};
    newObject.name = team;
    newObject.data = [];
    values.push(newObject);
  }

  // Filling the array
  for (let teamName of allTeams) {
    for (let year in data) {
      if (data[year][teamName] == undefined) {
        for (let index in values) {
          if (values[index].name == teamName) {
            values[index].data.push(0);
          }
        }
      } else {
        let value = parseInt(data[year][teamName]);
        for (let index in values) {
          if (values[index].name == teamName) {
            values[index].data.push(value);
          }
        }
      }
    }
  }
  return { key, values };
}

function playerOfTheMatchHelper(data) {
  let keys = Object.keys(data);
  let values = [];
  let names = [];
  for (let element in data) {
    let inner = [];
    let key = Object.keys(data[element]);
    let value = parseInt(data[element][key[0]]);
    inner.push(key[0]);
    names.push(key[0]);
    inner.push(value);
    values.push(inner);
  }
  return { keys, values, names };
}
