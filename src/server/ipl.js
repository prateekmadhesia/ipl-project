function matchPerYear(matchData) {
  let result = matchData.reduce((object, element) => {
    let year = element.season;
    if (year in object) {
      object[year] += 1;
    } else {
      object[year] = 1;
    }
    return object;
  }, {});

  return result;
}

function matchWonPerYear(matchData) {
  const result = matchData.reduce((object, element) => {
    let year = element.season;
    let winner = element.winner;

    if (winner == "") {
      winner = "Draw";
    }
    if (year in object) {
      if (winner in object[year]) {
        object[year][winner] += 1;
      } else {
        object[year][winner] = 1;
      }
    } else {
      object[year] = {};
      object[year][winner] = 1;
    }
    return object;
  }, {});

  return result;
}

function extraRuns(matches, deliveries) {
  let matchId = matches
    .filter((element) => {
      if (element.season == 2016) {
        return element.id;
      }
    })
    .map((filteredData) => {
      return filteredData.id;
    });

  let result = deliveries.reduce((object, element) => {
    let find = matchId.find((value) => {
      return element.match_id == value;
    });
    if (find != undefined) {
      let bowlingTeam = element.bowling_team;
      let runs = parseInt(element.extra_runs);
      if (bowlingTeam in object) {
        object[bowlingTeam] += runs;
      } else {
        object[bowlingTeam] = runs;
      }
    }
    return object;
  }, {});
  return result;
}

function economicBowlers(matches, deliveries) {
  let average = {};
  let matchId = matches
    .filter((element) => {
      if (element.season == 2015) {
        return element.id;
      }
    })
    .map((filteredData) => {
      return filteredData.id;
    });

  deliveries.reduce((object, element) => {
    let value = matchId.find((data) => {
      return element.match_id == data;
    });
    if (value != undefined) {
      let run = parseInt(element.total_runs);
      let bowler = element.bowler;
      if (bowler in object) {
        object[bowler].balls += 1;
        object[bowler].runs += run;
      } else {
        object[bowler] = {};
        object[bowler].balls = 1;
        object[bowler].runs = run;
      }
      let over = object[bowler].balls / 6;
      average[bowler] = object[bowler].runs / over;
    }
    return object;
  }, {});

  //converted object to array
  const averageArray = Object.entries(average).sort(
    (firstValue, secondValue) => {
      return firstValue[1] - secondValue[1];
    }
  );
  let result = averageArray
    .filter((element, index) => {
      return index < 10;
    })
    .reduce((object, currentValue) => {
      let player = currentValue[0];
      let average = currentValue[1];
      object[player] = average;
      return object;
    }, {});

  return result;
}

function tossAndMatchWon(matches) {
  let result = matches.reduce((object, element) => {
    if (element.toss_winner == element.winner) {
      let winner = element.winner;
      if (winner in object) {
        object[winner] += 1;
      } else {
        object[winner] = 1;
      }
    }
    return object;
  }, {});
  return result;
}

function playerOfTheMatch(matches) {
  let result = {};
  let players = matches.reduce((object, element) => {
    let player = element.player_of_match;
    let year = element.season;
    if (year in object) {
      if (player in object[year]) {
        object[year][player] += 1;
      } else {
        object[year][player] = 1;
      }
    } else {
      object[year] = {};
      object[year][player] = 1;
    }
    return object;
  }, {});
  for (let years in players) {
    let playerName = "";
    let awardNumber = -1;
    let year = years;
    for (let player in players[years]) {
      let value = parseInt(players[year][player]);
      if (awardNumber < value) {
        playerName = player;
        awardNumber = value;
      }
    }
    result[year] = {};
    result[year][playerName] = awardNumber;
  }
  return result;
}

function strickRate(matches, deliveries) {
  let matchYear = matches.reduce((object, element) => {
    let id = element.id;
    let season = parseInt(element.season);
    object[id] = season;
    return object;
  }, {});

  let result = deliveries.reduce((playerData, element) => {
    let batsman = element.batsman;
    let id = element.match_id;
    let year = matchYear[id];

    if (playerData[batsman] != undefined) {
      if (playerData[batsman][year] != undefined) {
        playerData[batsman][year].ball += 1;
        playerData[batsman][year].run += parseInt(element.batsman_runs);
        playerData[batsman][year].strickRate =
          playerData[batsman][year].run / playerData[batsman][year].ball;
      } else {
        playerData[batsman][year] = {};
        playerData[batsman][year].ball = 1;
        playerData[batsman][year].run = parseInt(element.batsman_runs);
        playerData[batsman][year].strickRate =
          playerData[batsman][year].run / playerData[batsman][year].ball;
      }
    } else {
      playerData[batsman] = {};
      playerData[batsman][year] = {};
      playerData[batsman][year].ball = 1;
      playerData[batsman][year].run = parseInt(element.batsman_runs);
      playerData[batsman][year].strickRate =
        playerData[batsman][year].run / playerData[batsman][year].ball;
    }
    return playerData;
  }, {});

  return result;
}

function playerDismiss(deliveries) {
  let result = {};

  deliveries
    .filter((element) => {
      return element.player_dismissed.length;
    })
    .reduce((acc, curr) => {
      if (acc[curr.player_dismissed] != undefined) {
        acc[curr.player_dismissed] += 1;
      } else {
        acc[curr.player_dismissed] = 1;
      }
      return result;
    }, result);

  let playerName = "";
  let dismissNumber = -1;
  let resultPlayer = {};

  for (let player in result) {
    if (result[player] > dismissNumber) {
      playerName = player;
      dismissNumber = parseInt(result[player]);
    }
  }
  resultPlayer[playerName] = dismissNumber;
  return resultPlayer;
}

function economySuperOver(deliveries) {
  let result = {};

  let superOver = deliveries.filter((element) => {
    return element.is_super_over != 0;
  });

  let bowlers = superOver.reduce((acc, curr) => {
    let bowler = curr.bowler;
    let run = parseInt(curr.total_runs);

    if (acc[bowler] != undefined) {
      acc[bowler].runs += run;
      acc[bowler].balls += 1;
      acc[bowler].average = acc[bowler].runs / acc[bowler].balls;
    } else {
      acc[bowler] = {};
      acc[bowler].balls = 1;
      acc[bowler].runs = run;
      acc[bowler].average = run;
    }
    return acc;
  }, {});

  let resultBowler = "";
  let resultEconomy = 1000000000;
  for (let bowler in bowlers) {
    let bowlerEconomy = parseInt(bowlers[bowler].average);
    if (resultEconomy > bowlerEconomy) {
      resultEconomy = bowlerEconomy;
      resultBowler = bowler;
    }
  }
  result[resultBowler] = bowlers[resultBowler];

  return result;
}

module.exports = {
  matchPerYear: matchPerYear,
  matchWonPerYear: matchWonPerYear,
  extraRuns: extraRuns,
  economicBowlers: economicBowlers,
  tossAndMatchWon: tossAndMatchWon,
  playerOfTheMatch: playerOfTheMatch,
  strickRate: strickRate,
  playerDismiss: playerDismiss,
  economySuperOver: economySuperOver,
};
