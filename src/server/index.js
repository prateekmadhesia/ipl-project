const fs = require("fs");
const csvConvert = require("csvtojson");
const IPL = require("./ipl.js");
const matchPath = "src/data/matches.csv";
const deliveriesPath = "src/data/deliveries.csv";

let matchData;
function matchesCSVtoJSON(data) {
  csvConvert()
    .fromString(data)
    .then((jsonData) => {
      matchData = jsonData;
      return jsonData;
    })
    .then((matches) => {
      let matchPerYear = IPL.matchPerYear(matches);
      writeJSONFile("src/public/output/matchesPerYear.json", matchPerYear);

      let matchWonPerYear = IPL.matchWonPerYear(matches);
      writeJSONFile("src/public/output/matchesWonPerYear.json", matchWonPerYear);

      let tossAndMatchWon = IPL.tossAndMatchWon(matches);
      writeJSONFile("src/public/output/tossAndMatchWon.json", tossAndMatchWon);

      let playerOfTheMatch = IPL.playerOfTheMatch(matches);
      writeJSONFile("src/public/output/playerOfTheMatch.json", playerOfTheMatch);

    });
}

function deliveriesCSVtoJSON(data) {
  csvConvert()
    .fromString(data)
    .then((deliveriesData) => {
      let extraRunsIn2016 = IPL.extraRuns(matchData, deliveriesData);
      writeJSONFile("src/public/output/extraRunsIn2016.json", extraRunsIn2016);

      let top10EconomicBowlers = IPL.economicBowlers(matchData, deliveriesData);
      writeJSONFile("src/public/output/top10EconomicalBowlers.json", top10EconomicBowlers);

      let strickRate = IPL.strickRate(matchData, deliveriesData);
      writeJSONFile("src/public/output/strickRate.json", strickRate);

      let playerDismiss = IPL.playerDismiss(deliveriesData);
      writeJSONFile("src/public/output/playerDismiss.json", playerDismiss);

      let economySuperOver = IPL.economySuperOver(deliveriesData);
      writeJSONFile("src/public/output/economySuperOver.json", economySuperOver);
    });
}

fs.readFile(matchPath, "utf-8", (err, data) => {
  if (err) {
    console.error("error=", err);
  }
  matchesCSVtoJSON(data);
});

fs.readFile(deliveriesPath, "utf-8", (err, data) => {
  if (err) {
    console.error(err);
  }
  deliveriesCSVtoJSON(data);
});

function writeJSONFile(path, data){
  fs.writeFile(
    path,
    JSON.stringify(data),
    (err) => {
      if (err) {
        console.error(err);
        return;
      };
      console.log("File saved");
    }
  );
}